import Vue from 'vue'
import { getymd } from '@/common/js/utils/date.js'


Vue.filter('date', (timeStamp, format='Y-m-d H:i:s')=>{
	if(!timeStamp) return '';
	return getymd(format, timeStamp);
});

Vue.filter('friendDate', (timeStamp, format='Y-m-d')=>{
	if(!timeStamp) return '';
	return getymd(format, timeStamp);
});

Vue.filter('price', (price, fixed=0)=>{
	if(!price || isNaN(price)){
		return 0
	}
	return fixed > 0 ? parseFloat(price).toFixed(fixed) : + parseFloat(price).toFixed(2);
});

// 返回驿站距离
Vue.filter('dist', function(distance) {
	distance = parseFloat(distance)
	if(distance > 1000) return parseFloat(distance/1000).toFixed(2) + "公里"
	if(distance < 1000) {
		if(distance > 100) {
			return distance + "米"
		} else {
			return "就在附近"
		}
	} 
})