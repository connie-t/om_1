package com.zero2oneit.mall.feign.member;

import com.zero2oneit.mall.common.bean.member.MemberRetail;
import com.zero2oneit.mall.common.query.member.MemberRetailQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/12 19:27
 */
@FeignClient("member-service")
public interface MemberRetailFeign {

    @PostMapping("/admin/member/retail/list")
    BoostrapDataGrid list(@RequestBody MemberRetailQueryObject qo);

    @PostMapping("/admin/member/retail/addOrEdit")
    R addOrEdit(@RequestBody MemberRetail retail);
}
