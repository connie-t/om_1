const env = require('./upload.config.js'); //配置文件，在这文件里配置你的OSS keyId和KeySecret,timeout:87600;
const uploadFile = require('./uploadCommon.js'); //配置文件，在这文件里配置你的OSS keyId和KeySecret,timeout:87600;

const chooseVideo = function (count, url="advert/",successc, failc) {
	const aliyunServerURL = env.uploadImageUrl;//OSS地址，需要https
	uni.chooseVideo({
		// ablum相册 camera相机 ，默认二者都有
		sourceType: ['album', 'camera'],
		// 最多可选图片张数，默认9
		count,
		// 成功则返回图片的本地文件路径列表 tempFilePaths
		success: res => {
			uni.showLoading({
				title:"上传中"
			});
			uploadFile(res.tempFilePath,url,result => {
				// #ifdef H5
				result = result.replace('/oss','阿里云链接');
				// #endif
				successc(result);
				//成功返回
				uni.hideLoading()
				}, fail => { //失败返回
					uni.showToast({ title: '视频数据太大，请压缩后重新上传', icon: "none"});
					uni.hideLoading()
				},'video'
			)
		},
		// 接口调用失败的回调函数
		fail: err => {}
	});
};
module.exports = chooseVideo;